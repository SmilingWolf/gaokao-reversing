def gaokaoNameEncrypt(string):
	total = 0
	for letter in string:
		total = ((total * 0x1000193) & 0xFFFFFFFF) ^ ord(letter)
	return total

hashesList = []
for line in open('hashtoCrack.txt', 'r'):
	hashesList.append(int(line.rstrip(), 16))

prefix = 'background'
intervallo = 1000
variations = ['', 'ef', 'aef', 'eff', 'blur']
extensions = ['.ogg', '.png', '.jpg']
for i in xrange(0, intervallo):
	for variation in variations:
		for ext in extensions:
			fileName = ('%s%03d%s%s' % (prefix, i, variation, ext)).encode('utf-16le')
			hashVal = gaokaoNameEncrypt(fileName)
			#print fileName.decode('utf-16le')
			if hashVal in hashesList:
				print '%08X -> %s' % (hashVal, fileName.decode('utf-16le'))
			#if i % 1000 == 0:
			#	print '%s' % (fileName.decode('utf-16le'))

#for testName in open('clearList.txt', 'r').readlines():
#	testName = testName.rstrip()
#	fileName = testName.decode('utf-8').encode('utf-16le')
#	hashVal = gaokaoNameEncrypt(fileName)
#	#print fileName.decode('utf-16le')
#	if hashVal in hashesList:
#		print '%08X -> %s' % (hashVal, testName)
#	#if i % 1000 == 0:
#	#	print '%s' % (fileName.decode('utf-16le'))
