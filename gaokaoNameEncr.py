def gaokaoNameEncrypt(string):
	total = 0
	for letter in string:
		total = ((total * 0x1000193) & 0xFFFFFFFF) ^ ord(letter)
	return total

name = '.tjs'.encode('utf-16le')
print '%08X' % gaokaoNameEncrypt(name)
