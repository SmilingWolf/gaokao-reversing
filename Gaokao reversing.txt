ab50a256db9a6717de162757: XOR key: 0x3F820210 1DEF5BA300CA4100
ab53b64df0362acede15334c: XOR key: 0xDEBC45BF 1DEF5BA300CA4100

f202035ba61a7df58744865a: XOR key: 0x773A22F3 1DEF5BA300CA4100
722d3f38b7bbbdef6b309cdb: XOR key: 0x6E9BA986 1DEF5BA300CA4100

717fad30e13a428b68620ed3: XOR key: 0xAD81764D 1DEF5BA300CA4100


Filenames: unknown so far
Files XORed with a key 0x0C bytes long, composed like 
unsigned int key[3]
key[0] = fileHeader->adlr
key[1] = 0x1DEF5BA3
key[2] = 0x00CA4100








others.xp3:
>dialog_about.png -> adler32: 0624B131
fakeFileName: 87 7B D6 4D 50 99 06 A0 F2 3D 53 4C
50 99 06 A0 ^ 87 7B D6 4D = D7E2D0ED
F2 3D 53 4C ^ 87 7B D6 4D = 75468501

others.xp3:
>title2_bgd.png
fakeFileName: 301db44415eae076455b3145
15eae076 ^ 301db444 = 25F75432
455b3145 ^ 301db444 = 75468501

data.xp3:
>main/option.ks


data.xp3:
startup.tjs



handle: 138


RVA: 2205A0: sta leggendo il virtual file system

RVA: 220B83: la prima DWORD di delle 3 al fondo del segmento info è usata come chiave per lo XOR delle altre 2, per cui:
Seconda File entry nell'header di data.xp3:
DWORD0: 9F75C258
DWORD1: 54685683
DWORD2: 8636ED10
54685683 ^ 9F75C258 = CB1D94DB
8636ED10 ^ 9F75C258 = 19432F48

RVA: 246759: Qui sotto sta facendo lo strLower del nome che ci interessa
RVA:  11CA4: Qui sta calcolando un qualche checksum sul nome del file che gli interessa
Per il nome 'startup.tjs' in UTF-16 in checksum in EAX è: A657D74B

data.xp3:
startup.tjs:
NameSum = 4B D7 57 A6 or 0xA657D74B
segm.offset: 26 20 73 00
FakeFileName: D6 5C 93 47 3C 92 0E 68 CF 1F BC 0F
3C 92 0E 68 ^ D6 5C 93 47 = EACE9D2F -> 0x2F9DCEEA
CF 1F BC 0F ^ D6 5C 93 47 = 19432F48 -> 0x482F4319

Una entry con gli offset (seconda DWORD):
02C2420C  90 10 C2 02 26 20 73 00 00 00 00 00 00 00 00 00  ..Â.& s.........  
02C2421C  00 00 00 00 C0 02 00 00 00 00 00 00 39 02 00 00  ....À.......9...  
02C2422C  00 00 00 00 01 FA 18 00                          .....ú..

Fa anche il NameSum del percorso completo dell'archivio:
file://./f/steamlibrary/steamapps/common/gaokao100/data.xp3 -> 9F 98 9F 32 or 0x329F989F

Fa anche il NameSum del percorso completo del file richiesto:
file://./f/steamlibrary/steamapps/common/gaokao100/data.xp3>startup.tjs -> 22 23 CC 85 or 0x85CC2322

0062E755: Riassunto:
[ESP] -> [...+20] = [[ESP]+20]: qui troviamo l'offset

0062E5E4: Riassunto:
In EAX+4 verrà messo l'indirizzo dell'entry con l'offset richiesto

0062E668: Ritorna in EAX l'indirizzo dell'entry con l'offset richiesto

00622D02: Questa chiamata ritorna in EAX il puntatore ad una struttura che verrà riempita più avanti
Una volta riempita si fa [EAX+20] et voilà, habemus offset

00622D2B: Riempie la struttura ritornata qui sopra da 00622D02

Struttura dell'entry con il pointer all'offset:
02B6C9E0                                      EA CE 9D 2F              êÎ./  
02B6C9F0  44 80 00 00 F8 FB 18 00 19 43 2F 48 D1 90 B3 19  D...øû...C/HÑ.³.  
02B6CA00  C0 02 00 00 00 00 00 00 39 02 00 00 00 00 00 00  À.......9.......  
02B6CA10  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................  
02B6CA20  10 42 BE 02 34 42 BE 02 34 42 BE 02 00 00 00 00  .B¾.4B¾.4B¾.....  

La prima DWORD è quella generata a partire da FakeFileName
La 14° DWORD (a 02B6CA20) è il pointer all'offset

00646BD4: Potrebbe essere importante?
00644724: Chiamata poco sopra quella che ho scritto ^
          Fa dei calcoli sul nome del file

Funzione per il calcolo del nameHash, ovvero FakeFileName[1] e FakeFileName[2]:
00644724 | 53                       | push ebx                               |
00644725 | 8BCA                     | mov ecx,edx                            |
00644727 | 03C8                     | add ecx,eax                            | eax:L"startup.tjs"
00644729 | 33D2                     | xor edx,edx                            |
0064472B | 3BC8                     | cmp ecx,eax                            | eax:L"startup.tjs"
0064472D | 76 13                    | jbe gaokao100.nosteam.644742           |
0064472F | 69DA 93010001            | imul ebx,edx,1000193                   |
00644735 | 89DA                     | mov edx,ebx                            |
00644737 | 33DB                     | xor ebx,ebx                            |
00644739 | 8A18                     | mov bl,byte ptr ds:[eax]               | eax:L"startup.tjs"
0064473B | 33D3                     | xor edx,ebx                            |
0064473D | 40                       | inc eax                                | eax:L"startup.tjs"
0064473E | 3BC8                     | cmp ecx,eax                            | eax:L"startup.tjs"
00644740 | 77 ED                    | ja gaokao100.nosteam.64472F            |
00644742 | 8BC2                     | mov eax,edx                            | eax:L"startup.tjs"
00644744 | 5B                       | pop ebx                                |
00644745 | C3                       | ret                                    |

In python:
----
def gaokaoNameEncrypt(string):
	total = 0
	for letter in name:
		total = ((total * 0x1000193) & 0xFFFFFFFF) ^ ord(letter)
	return total

name = 'startup.tjs'.encode('utf-16le')
print '%08X' % gaokaoNameEncrypt(name)
----
Ritorna 2F9DCEEA per startup.tjs in UTF-16LE

Actually,
FakeFileName[1] è l'hash del nome completo del file
FakeFileName[2] è l'hash dell'estensione, punto (.) compreso, per cui
----
def gaokaoNameEncrypt(string):
	total = 0
	for letter in name:
		total = ((total * 0x1000193) & 0xFFFFFFFF) ^ ord(letter)
	return total

name = '.tjs'.encode('utf-16le')
print '%08X' % gaokaoNameEncrypt(name)
----
Ritorna 482F4319 per .tjs in UTF-16LE

Compilation di hash utili:
482F4319 -> .tjs
B7B8698B -> .bmp
E3A31D19 -> .jpg
01854675 -> .png
58924012 -> .txt
